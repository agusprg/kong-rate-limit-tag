-- Copyright (C) Kong Inc.
local timestamp = require "kong.tools.timestamp"
local policies = require "kong.plugins.rate-limiting-tag.policies"

local kong = kong
local ngx = ngx
local max = math.max
local time = ngx.time
local floor = math.floor
local pairs = pairs
local error = error
local tostring = tostring
local timer_at = ngx.timer.at

local EMPTY = {}
local EXPIRATION = require "kong.plugins.rate-limiting-tag.expiration"

local RATELIMIT_LIMIT     = "RateLimit-Limit"
local RATELIMIT_REMAINING = "RateLimit-Remaining"
local RATELIMIT_RESET     = "RateLimit-Reset"
local RETRY_AFTER         = "Retry-After"
local RATELIMIT_RETRY_COUNTDOWN = "x-rate-limit-countdown"
local KEY_ENCRYPT = "Tsel"

local X_RATELIMIT_LIMIT = {
   second = "X-RateLimit-Limit-Second",
   minute = "X-RateLimit-Limit-Minute",
   hour   = "X-RateLimit-Limit-Hour",
   day    = "X-RateLimit-Limit-Day",
   month  = "X-RateLimit-Limit-Month",
   year   = "X-RateLimit-Limit-Year",
}

local X_RATELIMIT_REMAINING = {
   second = "X-RateLimit-Remaining-Second",
   minute = "X-RateLimit-Remaining-Minute",
   hour   = "X-RateLimit-Remaining-Hour",
   day    = "X-RateLimit-Remaining-Day",
   month  = "X-RateLimit-Remaining-Month",
   year   = "X-RateLimit-Remaining-Year",
}
local RateLimitingHandler = {}

RateLimitingHandler.PRIORITY = 901
RateLimitingHandler.VERSION = "2.4.0"
local reachlimit = false

local function get_identifier(conf)
   local identifier
   conf.limit_by = "service"
   if conf.limit_by == "service" then
      identifier = (kong.router.get_service() or
      EMPTY).id
   elseif conf.limit_by == "consumer" then
      identifier = (kong.client.get_consumer() or
      kong.client.get_credential() or
      EMPTY).id

   elseif conf.limit_by == "credential" then
      identifier = (kong.client.get_credential() or
      EMPTY).id

   elseif conf.limit_by == "header" then
      identifier = kong.request.get_header(conf.header_name)

   elseif conf.limit_by == "x-msisdn" then
      local key = kong.request.get_header("x-msisdn")
      local partition = conf.partition
      identifier = key % partition

   elseif conf.limit_by == "path" then
      local req_path = kong.request.get_path()
      if req_path == conf.path then
         identifier = req_path
      end
   end
   --kong.log.notice("RLTAG=identifier:",identifier)
   return identifier or kong.client.get_forwarded_ip()
end



function RateLimitingHandler:init_worker()
   --local conf, err = get_conf(self)
   --local ok, err = ngx.timer.every(5, scheduler)
   --if not ok then
   -- ngx.log(ngx.ERR, "failed to create the timer: ", err)
   --return
   --end
end


local function get_usage(conf, identifier, current_timestamp, limits)
   local usage = {}
   local stop

   for period, limit in pairs(limits) do
      --local current_usage, err = policies[conf.policy].usage(conf, identifier, period, current_timestamp)
      conf.policy="local"
      local current_usage, err = policies[conf.policy].usage(conf, identifier, period, current_timestamp)
      if err then
         return nil, nil, err
      end

      -- What is the current usage for the configured limit name?
      local remaining = limit - current_usage
      -- kong.log.err("remaining usage-----> ", tostring(remaining),"currren",tostring(current_usage))

      -- Recording usage
      usage[period] = {
         limit = limit,
         remaining = remaining,
      }

      if remaining <= 0 then
         stop = period
      end
   end

   return usage, stop
end


local function increment(premature, conf, ...)
   if premature then
      return
   end

   policies[conf.policy].increment(conf, ...)
end




function RateLimitingHandler:header_filter(conf)
   local ctx = kong.ctx.plugin
   if ctx.reject then
      kong.log.err("RLTAG=limit-noupdate-tag")
      return
   end
   local headers = kong.response.get_headers()
   local current_timestamp = time() * 1000
   local tag_value = conf.tag_value_add .. "@" .. current_timestamp
   local tag_valuex = enc(tag_value)
   
   kong.log.err("RLTAG=add-tag,",tag_value,":",tag_valuex)
   kong.response.set_header(conf.tag_header, tag_valuex)
end

function RateLimitingHandler:access(conf)
   local ctx = kong.ctx.plugin

   local req_headers = kong.request.get_headers()
   local req_query = kong.request.get_query()
   local myheader=req_headers[conf.tag_header]
   --local mydeviceid=req_headers["deviceid"]
   --kong.log.notice("RLTAG1=",conf.tag_header,":",myheader,",deviceid:",mydeviceid)
   if conf.tag_only then
      kong.log.notice("RLTAGE=tag-only")
      return
   end
   local current_timestamp = time() * 1000
 
   -- Consumer is identified by ip address or authenticated_credential id
   local identifier = get_identifier(conf)
   local fault_tolerant = conf.fault_tolerant

   -- Load current metric for configured period
   local limits = {
      second = conf.trx_per_second,
      minute = conf.trx_per_minute,
      hour = conf.hour,
      day = conf.day,
      month = conf.month,
      year = conf.year,
   }

   if limits.second==0 then
      limits.second=nil
   end
   if limits.minute==0 then
      limits.minute=nil
   end

   --kong.log.err("RLTAG=",limits.second,";",limits.minute,";",limits.hour)
   local req_headers = kong.request.get_headers()
   local req_query = kong.request.get_query()

   local usage, stop, err = get_usage(conf, identifier, current_timestamp, limits)
   if err then
      if not fault_tolerant then
         return error(err)
      end

      kong.log.err("failed to get usage: ", tostring(err))
   end

   if usage then
      -- Adding headers
      local reset
      local headers
      if not conf.hide_client_headers then
         headers = {}
         local timestamps
         local limit
         local window
         local remaining
         for k, v in pairs(usage) do
            local current_limit = v.limit
            local current_window = EXPIRATION[k]
            local current_remaining = v.remaining
            -- kong.log.err("usage,limit: ", tostring(current_limit),",window:", tostring(current_window), ",remaining:",tostring(current_remaining) )
            if stop == nil or stop == k then
               current_remaining = current_remaining - 1
            end
            current_remaining = max(0, current_remaining)

            if not limit or (current_remaining < remaining)
            or (current_remaining == remaining and
            current_window > window)
            then
               limit = current_limit
               window = current_window
               remaining = current_remaining

               if not timestamps then
                  timestamps = timestamp.get_timestamps(current_timestamp)
               end

               reset = max(1, window - floor((current_timestamp - timestamps[k]) / 1000))
            end

            headers[X_RATELIMIT_LIMIT[k]] = current_limit
            headers[X_RATELIMIT_REMAINING[k]] = current_remaining
         end

         headers[RATELIMIT_LIMIT] = limit
         headers[RATELIMIT_REMAINING] = remaining
         headers[RATELIMIT_RESET] = reset
         headers[RATELIMIT_RETRY_COUNTDOWN] = conf.tag_rate_limit_countdown

      end

      -- stop = true
      if stop then
         headers = headers or {}
         headers[RETRY_AFTER] = reset
         headers[RATELIMIT_RETRY_COUNTDOWN] = conf.tag_rate_limit_countdown
       
         --global variable
         reachlimit = true
         local current_timestamp = time() * 1000
         local header = req_headers[conf.tag_header]
         --local deviceid = req_headers['deviceid']
         --kong.log.err("RLTAG=tag_allow_check1:",conf.tag_header,":",header,",deviceid:",deviceid)
         ctx.reach = true
         if header ~=nil then
            -- decrypt
            local headerx=dec(header)
            kong.log.err("RLTAG=decrypt:",header,":",headerx,"X")

            local tag, time = headerx:match("([^,]+)@([^,]+)")
            local tag_ttl =  tonumber(conf.tag_ttl)
            local timestamps = tonumber(time)
            local time_check = 0
            --kong.log.err("RLTAG=tag_allow_check2,time:",time)
            if time ~=nil then
               -- timestamps = timestamp.get_timestamps(time)
               time_check = floor((current_timestamp - timestamps) / 1000)
               kong.log.err("time_check ",current_timestamp,"-",timestamps,"=",time_check)
            end
            --check *

            for _, checkHeader in ipairs(conf.tag_allow) do
               --kong.log.err("RLTAG=tag_allow_check3,header:", checkHeader)
                if checkHeader == tag then
                  --kong.log.err("RLTAG=ag_allow_check4:",checkHeader,";",time_check, "<=>",tag_ttl,"=TRUE",",deviceid:",deviceid)
                  if time_check > tag_ttl then
                     ctx.reject = true
                     --kong.log.err("RLTAG=tag_allow_check:",checkHeader, "=EXPIRED",",deviceid:",deviceid)
                     return kong.response.exit(conf.status_code, conf.response_body, headers)
                  end
                end
            end

         end
      end
      if headers then
         kong.response.set_headers(headers)
      end
   end
   local ok, err = timer_at(0, increment, conf, limits, identifier, current_timestamp, 1)
   if not ok then
      kong.log.err("failed to create timer: ", err)
   end
end

-- encoding
function enc(data)
   local l=string.len(data)
   local toleft=l/2
   return ngx.encode_base64(data:sub(toleft+1)..data:sub(1,toleft))
end

-- decoding
function dec(data)
   data=ngx.decode_base64(data)
   local l=string.len(data)
   local toright=l/2
   return data:sub(data:len()-toright+1)..data:sub(1,data:len()-toright)
end

return RateLimitingHandler